<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class HomeController extends Controller
{
    // public function gitlab1()
    // {
    //     return view('article/gitlab1');
    // }
    // public function gitlab2()
    // {
    //     return view('article/gitlab2');
    // }
    // public function gitlab3()
    // {
    //     return view('article/gitlab3');
    // }
    public function gitlab()
    {
        $client = new Client();
        $uri = "https://gitlab.com/api/v4/users/adram72/projects";
        $header = array('headers' => array('PRIVATE-TOKEN' => 'KNGFFAY5GyBU92jzt2zo'));
        $response = $client->get($uri, $header);
        // dd($respons->getBody()->getContents());
        $gitlabs = json_decode($response->getBody()->getContents());
        return view('article.gituser', compact('gitlabs'));
    }
    public function user1()
    {
        $client = new Client();
        $uri = "https://gitlab.com/api/v4/users/IsnanNazarun/projects";
        // $header = array('headers' => array('PRIVATE-TOKEN' => 'KNGFFAY5GyBU92jzt2zo'));
        $response = $client->get($uri);
        // dd($respons->getBody()->getContents());
        $gitlabs = json_decode($response->getBody()->getContents());
        return view('article.gitlab1', compact('gitlabs'));
    }
    public function user2()
    {
        $client = new Client();
        $uri = "https://gitlab.com/api/v4/users/ekahadiseprizal/projects";
        // $header = array('headers' => array('PRIVATE-TOKEN' => 'KNGFFAY5GyBU92jzt2zo'));
        $response = $client->get($uri);
        // dd($respons->getBody()->getContents());
        $gitlabs = json_decode($response->getBody()->getContents());
        return view('article.gitlab2', compact('gitlabs'));
    }
    public function user3()
    {
        $client = new Client();
        $uri = "https://gitlab.com/api/v4/users/mfajar/projects";
        // $header = array('headers' => array('PRIVATE-TOKEN' => 'KNGFFAY5GyBU92jzt2zo'));
        $response = $client->get($uri);
        // dd($respons->getBody()->getContents());
        $gitlabs = json_decode($response->getBody()->getContents());
        return view('article.gitlab3', compact('gitlabs'));
    }
}