@extends('layout.master')
@section('content')
<table class="table table-stripped">
    <tr>
        <th> No. </th>
        <th> Nama </th>
        <th> Project </th>
        <th> Link </th>
    </tr>
    @foreach($gitlabs as $item)
    <tr>
        <td>{{ $loop->iteration}}</td>
        <td>{{ $item->name }} </a> </td>
        <td>{{ $item->name_with_namespace }}</td>
        <td>{{ $item->web_url }}</td>
    </tr>
    @endforeach
</table>
@endsection