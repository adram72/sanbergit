# Soal SanberGit

## Tujuan
- membuat aplikasi dari API gitlab
- menggunakan guzzle http di project laravel

## Petunjuk Pengerjaan
Buatlah sebuah project baru laravel bernama sanbergit. Di dalam project tersebut digunakan personal access-token gitlab masing-masing. Aplikasi yang akan kita buat adalah sebuah website untuk mempermudah kita dalam mengatur project yang kita miliki di gitlab.
Dokumentasi gitlab untuk menu projects bisa diperoleh dari link berikut: https://docs.gitlab.com/ee/api/projects.html

buatlah website tersebut dengan menu-menu sebagai berikut
1. Menampilkan project yang kita miliki di gitlab
- Tampilkanlah list repo/project yang kita miliki di gitlab di halaman utama website(route '/home' atau route '/'). 

2. Menampilkan project user lain
- Menu ini untuk menampilkan project user lain dengan cara mencari tahu dari username nya.

3. Membuat Project repo baru
- Menu ini akan menghasilkan repo baru di gitlab kita. 

4. Memberikan bintang (star) pada sebuah repo/projects tertentu
- menu star mirip dengan fitur like. 